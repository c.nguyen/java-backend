# Configuration
Dockerfile :
FROM alpine:latest

RUN apk add bash git curl zip openjdk11 maven

To configure the datasource :
/root/form-management/src/main/resources/application.properties

To manage dependencies :
/root/form-management/pom.xml

Download vs extension to use with Spring boot :
- Spring Boot Extension
- Project Manager

Procedures :
- ctrl + shift + p to select spring initializer to create maven project
- enter "package.name" to create a package
- enter "form-management" to create an id
- select data rest, pja, postgres, lombok depedencies
- put in /root/
- open
- execute to verify 